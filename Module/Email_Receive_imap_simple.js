var imaps = require('imap-simple');
const simpleParser = require('mailparser').simpleParser;
const _ = require('lodash');
const myConnection = require('./Query');
const logger = require('./logger');
const random_string = require('./random_string');
const moment = require('moment');
const tlsOptions = { rejectUnauthorized: false };
const saveAttachment = require('./SaveAttachment')
const configurationFile = require('../config.json');

const readEmail = async function () {
    try {
        logger("Get.Email.Account.Inbound");
        const data = await myConnection.getEmailAccountInbound();

        for (let i = 0; i < data.length; i++) {
            logger(`user = ${data[i].username}`);
            logger(`password = ${data[i].password}`);
            logger(`host = ${data[i].host}`);
            logger(`port = ${data[i].port}`);
            logger(`tls = ${data[i].tls}`);

            const configs = {
                imap: {
                    user: data[i].username,
                    password: data[i].password,
                    host: data[i].host,
                    port: data[i].port,
                    tls: Boolean(data[i].tls),
                    tlsOptions: tlsOptions
                }
            }

            await imaps.connect(configs).then(function (connection) {
                logger("Imaps.Running");
                const oneHourAgo = new Date();
                oneHourAgo.setHours(oneHourAgo.getHours() - 1);

                return connection.openBox('INBOX').then(function () {
                    var searchCriteria = ['UNSEEN', ['SINCE', oneHourAgo.toISOString()]];
                    var fetchOptions = {
                        bodies: ['HEADER', 'TEXT', ''],
                        markSeen: true,
                        struct: true
                    };
                    return connection.search(searchCriteria, fetchOptions).then(function (messages) {
                        var attachments = [];
                        messages.forEach(function (item) {
                            var all = _.find(item.parts, { "which": "" })
                            // var id = item.attributes.uid;
                            var id = random_string(15);
                            var idHeader = "Imap-Id: " + id + "\r\n";
                            simpleParser(idHeader + all.body, (err, mail) => {
                                // access to the whole mail object
                                var varTo = '';
                                var varCC = '';
                                var tanggal = moment(mail.date).format('YYYY-MM-DD HH:mm:ss');

                                for (let i = 0; i < mail.to.value.length; i++) {
                                    varTo += mail.to.value[i].address + ";"
                                }

                                if (mail.cc) {
                                    logger(`CC=${JSON.stringify(mail.cc)}`)
                                    for (let i = 0; i < mail.cc.value.length; i++) {
                                        varCC += mail.cc.value[i].address + ";"
                                    }
                                }

                                logger("ID = " + id);
                                logger("From = " + mail.from.value[0].address)
                                logger("To = " + varTo)
                                logger("CC = " + varCC)
                                logger("Date = " + tanggal)
                                logger("Subject = " + mail.subject)
                                logger("Body = " + mail.html)

                                myConnection.saveEmail(id,
                                    configs.imap.user,
                                    varTo,
                                    mail.from.value[0].address,
                                    varCC,
                                    mail.subject,
                                    tanggal,
                                    mail.html);
                            });

                            /*
                            * Attachments
                            */
                            logger(`EmailId=${id}:Check.Attachments`);
                            logger(`message.struct=${item.attributes.struct}, JSON=${JSON.stringify(item.attributes.struct)}`);
                            var parts = imaps.getParts(item.attributes.struct);
                            attachments = attachments.concat(parts.filter(function (part) {
                                logger(`EmailId=${id}:Find.Attachments.`);
                                return part.disposition && part.disposition.type.toUpperCase() === 'ATTACHMENT';
                            }).map(function (part) {
                                // retrieve the attachments only of the messages with attachments
                                logger(`EmailId=${id}:Attachments.Founded.`);
                                return connection.getPartData(item, part)
                                    .then(function (partData) {
                                        var filenameEncrypt = random_string(10) + "_" + part.disposition.params.filename;
                                        logger(`Id=${id}`)
                                        logger(`Filename=${filenameEncrypt}`)
                                        myConnection.saveAttachment(id, configurationFile.Attachments.Path, filenameEncrypt);
                                        saveAttachment.saveAttachments(filenameEncrypt,
                                            configurationFile.Attachments.Path,
                                            partData)
                                        return {
                                            filename: part.disposition.params.filename,
                                            data: partData,
                                            id: id
                                        };
                                    });
                            }));
                        });
                        logger("Exit");
                        connection.end();
                    });
                });
            });
        }

    } catch (error) {
        logger("ERROR!:ReadEmail.Msg=" + error.message);
    }
}

const runEmailreceive = async function () {
    await readEmail();
    setTimeout(runEmailreceive, 10000);
}

module.exports = {
    readEmail,
    runEmailreceive
}
